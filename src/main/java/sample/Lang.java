package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Lang extends Application {
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setWidth(350);
        primaryStage.setHeight(270);
        primaryStage.setResizable(false);
        AnchorPane pane = new AnchorPane();
        Button submit = new Button();
        submit.setText("SUBMIT");
        submit.setLayoutY(200);
        AnchorPane.setLeftAnchor(submit,0.0);
        AnchorPane.setRightAnchor(submit,0.0);

        ToggleButton en = new ToggleButton("EN");
        en.setText("EN");
        ToggleButton hu = new ToggleButton("HU");
        hu.setText("HU");
        ToggleGroup lang = new ToggleGroup();
        en.setToggleGroup(lang);
        hu.setToggleGroup(lang);
        hu.setLayoutX(204.0);
        hu.setLayoutY(80.0);
        AnchorPane.setLeftAnchor(hu,204.0);
        AnchorPane.setRightAnchor(hu,116.0);

        en.setLayoutX(116.0);
        en.setLayoutY(80.0);
        AnchorPane.setLeftAnchor(en,116.0);
        AnchorPane.setRightAnchor(en,204.0);

        submit.setOnAction(event -> {
            if (en.isSelected()) {
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("/sample-en.fxml"));
                    Main.stage = new Stage();
                    assert root != null;
                    Main.scene = new Scene(root, 600, 650);

                    Main.stage.setTitle("Youtube video downloader");
                    Main.stage.getIcons().add(new Image("/youtube.jpeg"));
                    Main.stage.setResizable(false);
                    Main.stage.setScene(Main.scene);
                    Main.stage.show();
                    primaryStage.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Main.error(e);
                }
            }else if(hu.isSelected()) {
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("/sample-hu.fxml"));

                    Main.stage = new Stage();
                    assert root != null;
                    Main.scene = new Scene(root, 600, 650);
                    Main.stage.setTitle("Youtube videó letöltő");
                    Main.stage.getIcons().add(new Image("/youtube.jpeg"));
                    Main.stage.setResizable(false);
                    Main.stage.setScene(Main.scene);
                    Main.stage.show();
                    primaryStage.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Main.error(e);
                }
            }
        });
        pane.getChildren().addAll(submit,en,hu);
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}