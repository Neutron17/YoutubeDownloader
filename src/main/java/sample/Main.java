package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class Main extends Application {
    public static Stage stage;
    public static Scene scene;
    @Override
    public void start(Stage primaryStage) {
//        Parent root = null;
//        root = FXMLLoader.load(getClass().getResource("/sample-en.fxml"));
//        stage = primaryStage;
//        stage.setTitle("Youtube video downloader");
//        stage.getIcons().add(new Image("/youtube.jpeg"));
//        stage.setResizable(false);
//        assert root != null;
//        scene = new Scene(root,600,650);
//        stage.setScene(scene);
//        stage.show();
        new Lang().start(new Stage());
    }

    public static void main(String[] args) {
        try {
            launch(args);
        }catch(Throwable th) {
            error(th);
        }
    }

    public static void error(Throwable th) {
        try {
            try {
                File log = new File("err.txt");
                if (!log.exists()) {
                    log.createNewFile();
                }
                FileWriter fw = new FileWriter(log, true);
                StringWriter errors = new StringWriter();
                th.printStackTrace(new PrintWriter(errors));

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                fw.write(dtf.format(now) + "\t" + System.getProperty("os.name") + "\n" + errors.toString());
                fw.flush();
                fw.close();
                throw new Exception();
            }catch (Throwable e) {
                foo();
            }
        }catch (Throwable thh) {
            System.exit(0);
        }
    }
    static void foo() {
        JFrame frame = new JFrame("Fatal error");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1275,200);
        frame.add(new JLabel("The program exited with an unrecoverable error. " +
                "Contact the developer at sandormatyas17@gmail.com, please send me an error report located at "+Controller.Companion.getWorkDir()
                + "/err.txt"));
        frame.setVisible(true);
    }
}
